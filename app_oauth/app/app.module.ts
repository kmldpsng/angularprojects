import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { RategainComponent } from './rategain.component';
import { RategainSignInComponent } from './rategainsignin.component';
import { RategainDetailsComponent } from './rategaindetails.component';
import { myrouter } from './rategain.route';
import { MyRouteGuard } from './oauth.guard';
import { userservice } from './user.service';



@NgModule({
  declarations: [
    AppComponent, RategainComponent, RategainDetailsComponent, RategainSignInComponent
  ],
  imports: [
    BrowserModule, FormsModule, RouterModule.forRoot(myrouter)
  ],
  providers: [MyRouteGuard, userservice],
  bootstrap: [AppComponent]
})
export class AppModule { }

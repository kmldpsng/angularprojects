import { Component } from '@angular/core';

@Component({
  selector: 'app-details',
  template:`
<h1>User has been authenticated!</h1>
<a routerLink="/signin">Home</a>
  `
})
export class RategainDetailsComponent {
  name:string = 'Rategain Details';

}

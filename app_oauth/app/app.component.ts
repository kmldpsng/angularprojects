import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  {{name}}

<nav>
  <a routerLink="/signin">Sign In</a>
  <a routerLink="/details">Details</a>
</nav>

<router-outlet></router-outlet>
  `
})
export class AppComponent {
  name: string = 'Root';

}

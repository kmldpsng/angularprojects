import { RategainSignInComponent } from "./rategainsignin.component";
import { RategainDetailsComponent } from "./rategaindetails.component";
import { Routes } from "@angular/router";
import { MyRouteGuard } from "./oauth.guard";



export const myrouter: Routes = [
    {
      path: 'signin', component: RategainSignInComponent
    },
    {
      path: 'details', component: RategainDetailsComponent,canActivate:[MyRouteGuard]
    }
  ]
  

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {userservice} from './user.service';

@Component({
    selector: 'app-signin',
    template: `
 <form (submit)="loginValidate($event)">
 <div>
 <label>Username</label>
 <input type="text">
 </div>
 <div>
 <label>Password</label>
 <input type="password">
 </div>
 <div>
 <input type="submit">
 </div>
 </form>
  `
})
export class RategainSignInComponent {

    constructor(private myRoute: Router,private us: userservice ) {

    }

    loginValidate(temp: any) {
        var u = temp.target.elements[0].value;
        var p = temp.target.elements[1].value;
        if(u=='admin' && p=='pswd')
        {
            this.us.setUserLoggedIn();
            this.myRoute.navigate(['details']);
        }
        
        console.log(u, p);
    }
}

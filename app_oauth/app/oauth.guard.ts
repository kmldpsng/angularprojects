import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { userservice } from './user.service';

@Injectable()
export class MyRouteGuard implements CanActivate {
    constructor(private myRouter: Router, private us: userservice) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this.us.getUserLoggedIn()) {
            return true;
        }
        else
        {
            this.myRouter.navigate(['/signin']);
            return false;
        }
    }
}
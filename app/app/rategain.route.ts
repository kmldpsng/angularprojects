import { RategainSignInComponent } from "./rategainsignin.component";
import { RategainDetailsComponent } from "./rategaindetails.component";
import { Routes } from "@angular/router/src/config";



export const myrouter: Routes = [
    {
      path: 'signin', component: RategainSignInComponent
    },
    {
      path: 'details', component: RategainDetailsComponent
    }
  ]
  
  
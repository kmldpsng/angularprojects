import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { RategainComponent } from './rategain.component';
import { RategainSignInComponent } from './rategainsignin.component';
import { RategainDetailsComponent } from './rategaindetails.component';
import { myrouter } from './rategain.route';



@NgModule({
  declarations: [
    AppComponent, RategainComponent, RategainDetailsComponent, RategainSignInComponent
  ],
  imports: [
    BrowserModule, RouterModule.forRoot(myrouter)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
